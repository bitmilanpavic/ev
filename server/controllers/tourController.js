const Tour = require('./../models/tourModel');
const Rev = require('./../models/reviewModel');

exports.getTour = async (req, res, next) => {
  let query = Tour.findOne({
    slug: req.params.slug
  });
  query = query.populate({ path: 'reviews' });
  let documents = await query;

  res.status(200).send({
    status: 'success',
    results: documents.length,
    data: documents
  });
};

exports.getAllTours = async (req, res, next) => {
  const documents = await Tour.find({});

  res.status(200).send({
    status: 'success',
    results: documents.length,
    data: documents
  });
};
