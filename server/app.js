const express = require('express');
const cors = require('cors');
const app = express();

app.use(cors());
app.use('/api/v1/tours', require('./routes/tourRoutes'));

module.exports = app;
