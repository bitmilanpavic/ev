// File used for server configuration, error handling, environment variables
const dotenv = require('dotenv');
dotenv.config({ path: `./config.env` });
const mongoose = require('mongoose');

// 1 Connect to mongoose
mongoose
  .connect(process.env.MONGODB, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  })
  .then(con => console.log('connected to database'))
  .catch(err => console.log(err));

// 2 Include app
const app = require('./app');

// 3 Run app on port 3001
const port = process.env.port || 3001;
app.listen(port, () => {
  console.log(`app runing on port ${port}`);
});
