import React, { Component } from 'react';
import HorizontalScroll from 'react-scroll-horizontal';
import ReactMapGL, {
  Popup,
  Marker,
  WebMercatorViewport,
  FlyToInterpolator
} from 'react-map-gl';
import axios from 'axios';
import './tour.scss';

class Tour extends Component {
  constructor(props) {
    super(props);

    this.clearTimeout = null;
    this.map = React.createRef();
    this.reviews = React.createRef();

    this.state = {
      tour: [],
      showPopup: {},
      scrollZoom: false,
      viewport: {
        zoom: 5,
        width: '100%',
        height: '600px'
      }
    };
  }

  getTour() {
    axios
      .get(
        `${process.env.REACT_APP_API_URL}/api/v1/tours/${this.props.match.params.tourName}`
      )
      .then(result => {
        const showPopup = {};
        const tour = [result.data.data];

        tour[0].locations.forEach(location => {
          showPopup[location.description] = true;
        });

        this.setState({
          ...this.state,
          tour,
          showPopup
        });

        // Enable map zoom on scroll only when more than one location, due to plugin bug
        if (tour[0].locations.length > 1) {
          window.addEventListener('scroll', this.handleScrollToMap);
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  handleScrollToMap = () => {
    let map = this.map.current;
    map = map.getBoundingClientRect();

    if (window.pageYOffset - map.height > map.top) {
      this.zoomOnMapLocations();
      window.removeEventListener('scroll', this.handleScrollToMap);
    }
  };

  zoomOnMapLocations = () => {
    const coordinates = [];

    this.state.tour[0].locations.forEach(location => {
      let coor1 = location.coordinates[0];
      let coor2 = location.coordinates[1];
      coordinates.push([Number(coor1), Number(coor2)]);
    });

    const { longitude, latitude, zoom } = new WebMercatorViewport(
      this.state.viewport
    ).fitBounds(coordinates, {
      padding: {
        top: 200,
        bottom: 200,
        left: 150,
        right: 0
      },
      offset: [0, 0]
    });

    const viewport = {
      ...this.state.viewport,
      longitude,
      latitude,
      zoom,
      transitionDuration: 1200,
      transitionInterpolator: new FlyToInterpolator()
    };

    this.setState({
      ...this.state,
      viewport
    });
  };

  updateMapHeight = () => {
    clearTimeout(this.clearTimeout);

    this.clearTimeout = setTimeout(() => {
      const height = this.reviews.current.offsetHeight - 150 + 'px';

      const viewport = {
        ...this.state.viewport,
        height,
        width: '100%'
      };

      this.setState({
        ...this.state,
        viewport
      });
    }, 500);
  };

  componentDidMount() {
    this.getTour();
    window.addEventListener('resize', this.updateMapHeight);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScrollToMap);
    window.removeventListener('resize', this.updateMapHeight);
  }

  render() {
    return (
      <React.Fragment>
        {this.state.tour.map((tour, tourIndex) => (
          <React.Fragment key={tourIndex}>
            <section className='tour-header'>
              <img
                className='tour-header__image'
                src={'/img/tours/' + tour.imageCover}
                alt={tour.name}
              />

              <div className='tour-header__heading-container'>
                <h1 className='tour-header__heading'>
                  <span>{tour.name} tour</span>
                </h1>

                <div className='tour-header__details-container'>
                  <svg>
                    <use xlinkHref='/img/icons.svg#icon-clock'></use>
                  </svg>
                  <span>{tour.duration} days</span>

                  <svg>
                    <use xlinkHref='/img/icons.svg#icon-map-pin'></use>
                  </svg>
                  <span>{tour.startLocation.description}</span>
                </div>
              </div>
            </section>

            <section className='tour-description'>
              <div className='tour-description__left-container'>
                <div className='tour-description__left'>
                  <h2 className='tour-description__heading green-h2 mb-3-5'>
                    QUICK FACTS
                  </h2>
                  <div className='tour-description__left-detail'>
                    <svg>
                      <use xlinkHref='/img/icons.svg#icon-calendar'></use>
                    </svg>
                    <span>NEXT DATE</span>
                    <span>
                      {new Date(tour.startDates[0]).toLocaleString('en-us', {
                        month: 'long',
                        year: 'numeric'
                      })}
                    </span>
                  </div>

                  <div className='tour-description__left-detail'>
                    <svg>
                      <use xlinkHref='/img/icons.svg#icon-trending-up'></use>
                    </svg>
                    <span>DIFFICULTY</span>
                    <span>{tour.difficulty}</span>
                  </div>

                  <div className='tour-description__left-detail'>
                    <svg>
                      <use xlinkHref='/img/icons.svg#icon-user'></use>
                    </svg>
                    <span>PARTICIPANTS</span>
                    <span>{tour.maxGroupSize}</span>
                  </div>

                  <div className='tour-description__left-detail'>
                    <svg>
                      <use xlinkHref='/img/icons.svg#icon-trending-up'></use>
                    </svg>
                    <span>RATING</span>
                    <span>{tour.ratingsAverage}</span>
                  </div>

                  <h2 className='tour-description__left-heading green-h2 mb-3-5 mt-5'>
                    YOUR TOUR GUIDES
                  </h2>
                  {tour.guides.map((guide, guideKey) => {
                    let guideType = 'Tour guide';
                    guideType =
                      guide.role === 'lead-guide' ? 'Lead guide' : guideType;

                    return (
                      <div
                        key={guideKey}
                        className='tour-description__left-detail'
                      >
                        <img
                          src={'/img/users/' + guide.photo}
                          alt={guide.name}
                        />
                        <span>{guideType}</span>
                        <span>{guide.name}</span>
                      </div>
                    );
                  })}
                </div>
              </div>

              <div className='tour-description__right-container'>
                <h2 className='green-h2 mb-3-5'>About {tour.name} tour</h2>
                {tour.description.split('\n').map((paragraph, pIndex) => (
                  <p key={pIndex}>{paragraph}</p>
                ))}
              </div>
            </section>

            <section className='tour-pictures'>
              {tour.images.map((image, imgIndex) => (
                <div className='tour-pictures__box' key={imgIndex}>
                  <img
                    className='tour-pictures__img'
                    src={'/img/tours/' + image}
                    alt=''
                  />
                </div>
              ))}
            </section>

            <section className='tour-map' ref={this.map}>
              <ReactMapGL
                latitude={Number(tour.startLocation.coordinates[1].toFixed(2))}
                longitude={Number(tour.startLocation.coordinates[0].toFixed(2))}
                scrollZoom={this.state.scrollZoom}
                {...this.state.viewport}
                onViewportChange={viewport =>
                  this.setState({ ...this.state, viewport })
                }
                mapStyle={process.env.REACT_APP_MAP_STYLE}
                mapboxApiAccessToken={process.env.REACT_APP_MAP_TOKEN}
              >
                {tour.locations.map((location, locationIndex) => {
                  let longitude = Number(location.coordinates[0].toFixed(2));
                  let latitude = Number(location.coordinates[1].toFixed(2));

                  return (
                    <React.Fragment key={locationIndex}>
                      <Marker latitude={latitude} longitude={longitude}>
                        <div className='marker'>
                          <img src='/img/pin.png' alt='' />
                        </div>
                      </Marker>

                      {this.state.showPopup[location.description] && (
                        <Popup
                          latitude={latitude}
                          longitude={longitude}
                          closeButton={true}
                          closeOnClick={true}
                          onClose={() =>
                            this.setState({
                              ...this.state,
                              showPopup: {
                                ...this.state.showPopup,
                                [location.description]: false
                              }
                            })
                          }
                          anchor='bottom'
                          offsetLeft={-10}
                          offsetTop={-25}
                        >
                          <div>{location.description}</div>
                        </Popup>
                      )}
                    </React.Fragment>
                  );
                })}

                <button
                  onClick={() => {
                    if (!this.state.scrollZoom) {
                      this.setState({ ...this.state, scrollZoom: true });
                    }
                  }}
                  className='tour-map__scroll-button'
                >
                  Enable scroll
                </button>
              </ReactMapGL>
            </section>

            <section className='tour-reviews' ref={this.reviews}>
              <HorizontalScroll
                style={{ height: '40rem' }}
                reverseScroll={true}
              >
                {tour.reviews.map((review, reviewIndex) => (
                  <div className='tour-reviews__card' key={reviewIndex}>
                    <div className='tour-reviews__card-avatar'>
                      <img
                        className='tour-reviews__card-img'
                        src={'/img/users/' + review.user.photo}
                        alt={review.user.name}
                      />
                      <h6 className='tour-reviews__card-user'>
                        {review.user.name}
                      </h6>
                    </div>
                    <p className='tour-reviews__card-text'>{review.review}</p>
                    <div className='tour-reviews__card-rating'>
                      {[1, 2, 3, 4, 5].map((star, starIndex) => (
                        <svg
                          key={starIndex}
                          className={
                            review.rating >= star ? 'active' : 'inactive'
                          }
                        >
                          <use xlinkHref='/img/icons.svg#icon-star'></use>
                        </svg>
                      ))}
                    </div>
                  </div>
                ))}
              </HorizontalScroll>
            </section>

            <section className='tour-cta'>
              <div className='tour-cta__container'>
                <div className='tour-cta__images'>
                  <div className='tour-cta__images-logo'>
                    <img src='/img/logo-white.png' alt='Natours logo' />
                  </div>

                  <img
                    className='tour-cta__images-img --image-1'
                    src={'/img/tours/' + tour.images[1]}
                    alt=''
                  />

                  <img
                    className='tour-cta__images-img --image-2'
                    src={'/img/tours/' + tour.images[2]}
                    alt=''
                  />
                </div>

                <div className='tour-cta__content'>
                  <h2 className='green-h2'>What are you waiting for?</h2>
                  <p>
                    {tour.duration} days. 1 adventure. Infinite memories. Make
                    it yours today!
                  </p>
                </div>

                <div className='tour-cta__button'>
                  <button className='green-button'>Book tour now!</button>
                </div>
              </div>
            </section>
          </React.Fragment>
        ))}
      </React.Fragment>
    );
  }
}

export default Tour;
