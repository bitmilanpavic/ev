import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import './tours.scss';
import * as actions from '../../Actions/toursActions';

class Tours extends Component {
  componentDidMount() {
    if (!this.props.tours.length) {
      this.props.getTours();
    }
  }

  render() {
    return (
      <ul className='tours'>
        {this.props.tours.map((tour, index) => (
          <li className='tour' key={index}>
            <div className='tour__header'>
              <div className='tour__header-picture-container'>
                <span className='tour__header-picture-overlay'></span>
                <img
                  className='tour__header-picture'
                  src={'/img/tours/' + tour.imageCover}
                  alt='The northen lights'
                />
              </div>

              <h3 className='tour__header-title'>
                <span>{tour.name}</span>
              </h3>
            </div>

            <div className='tour__details'>
              <h4 className='tour__details-sub-heading'>
                {tour.difficulty + ' ' + tour.duration + '-day tour'}
              </h4>
              <p className='tour__details-summary'>{tour.summary}</p>

              <div className='tour__details-data'>
                <svg>
                  <use xlinkHref='/img/icons.svg#icon-map-pin'></use>
                </svg>
                <span>{tour.startLocation.description}</span>
              </div>

              <div className='tour__details-data'>
                <svg>
                  <use xlinkHref='/img/icons.svg#icon-calendar'></use>
                </svg>
                <span>
                  {new Date(tour.startDates[0]).toLocaleString('en-us', {
                    month: 'long',
                    year: 'numeric'
                  })}
                </span>
              </div>

              <div className='tour__details-data'>
                <svg>
                  <use xlinkHref='/img/icons.svg#icon-flag'></use>
                </svg>
                <span>{tour.locations.length + ' stops'}</span>
              </div>

              <div className='tour__details-data'>
                <svg>
                  <use xlinkHref='/img/icons.svg#icon-user'></use>
                </svg>
                <span>{tour.maxGroupSize + ' people'}</span>
              </div>
            </div>

            <div className='tour__footer'>
              <p className='tour__footer-per-person'>
                <span className='tour__footer-value'>{'$' + tour.price}</span>{' '}
                <span className='tour__footer-text'>per person</span>
              </p>
              <p className='tour__footer-ratings'>
                <span className='tour__footer-value'>
                  {tour.ratingsAverage}
                </span>{' '}
                <span className='tour__footer-text'>
                  rating ({tour.ratingsQuantity})
                </span>
              </p>
              <Link className='green-button' to={'/tour/' + tour.slug}>
                Details
              </Link>
            </div>
          </li>
        ))}
      </ul>
    );
  }
}

const mapStateToProps = state => {
  return {
    tours: state.toursReducer.tours
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getTours: () => dispatch(actions.getTours())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Tours);
