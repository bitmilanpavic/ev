import React from 'react';
import { Link } from 'react-router-dom';
import './header.scss';

const Headerr = () => {
  const user = false;

  let navUser = (
    <React.Fragment>
      <a className='header__link' href='/login'>
        Log In
      </a>
      <a className='header__link --signup' href='/signUp'>
        Sign Up
      </a>
    </React.Fragment>
  );

  if (user) {
    navUser = (
      <React.Fragment>
        <a className='header__link' href='/logout'>
          Log out
        </a>
        <a href='/profile'>
          <img class='nav__user-img' src='/img/users/user-1.jpg' alt='' />
          <span>Jonas</span>
        </a>
      </React.Fragment>
    );
  }

  return (
    <header className='header'>
      <nav className='header__nav-left'>
        <Link className='header__link' to='/'>
          All tours
        </Link>
      </nav>
      <Link className='header__logo-contaner' to='/'>
        <img className='header__logo' src='/img/logo-white.png' alt='' />
      </Link>
      <nav className='header__nav-right'>{navUser}</nav>
    </header>
  );
};

export default Headerr;
