import React from 'react';
import './footer.scss';

const Footer = () => {
  return (
    <footer className='footer'>
      <div className='footer__logo-container'>
        <img
          className='footer__logo'
          src='/img/logo-green.png'
          alt='Natour logo'
        />
      </div>

      <div className='footer__content'>
        <ul className='footer__nav'>
          <li>
            <a href='/about-us'>About us</a>
          </li>
          <li>
            <a href='/download-apps'>Download apps</a>
          </li>
          <li>
            <a href='/become-guide'>Become a guide</a>
          </li>
          <li>
            <a href='/carrers'>Careers</a>
          </li>
          <li>
            <a href='contants'>Contact</a>
          </li>
        </ul>
        <p className='footer__copyright'>
          © Irure dolor in reprehenderit in voluptate velit esse cillum dolore
          eu fugiat nulla pariatur
        </p>
      </div>
    </footer>
  );
};

export default Footer;
