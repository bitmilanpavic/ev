import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './../../styles/_index.scss';

import Header from '../header/Header';
import Footer from '../footer/Footer';
import Tours from '../../Containers/tours/Tours';
import Tour from '../../Containers/tour/Tour';

const App = () => {
  return (
    <BrowserRouter>
      <Header />
      <main>
        <Switch>
          <Route path='/' exact component={Tours} />
          <Route path='/tour/:tourName' component={Tour} />
          <Route render={() => <h1>Not found</h1>} />
        </Switch>
      </main>
      <Footer />
    </BrowserRouter>
  );
};

export default App;
