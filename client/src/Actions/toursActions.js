import axios from 'axios';
import * as actionTypes from './actionTypes';

export const getTours = () => {
  return dispatch => {
    axios.get(`${process.env.REACT_APP_API_URL}/api/v1/tours`).then(result => {
      dispatch({ type: actionTypes.GET_TOURS, payload: result.data.data });
    });
  };
};
