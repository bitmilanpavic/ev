import * as actionTypes from '../Actions/actionTypes';

const initialState = {
  tours: []
};

const toursReducer = (state = initialState, action) => {
  if (action.type === actionTypes.GET_TOURS) {
    return {
      ...state,
      tours: action.payload
    };
  }

  return state;
};

export default toursReducer;
